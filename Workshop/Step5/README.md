# Openvia Workshop 2022
## Desafio 2

![](../resources/challenge.jpg)

Imagina que no teu próximo trabalho académico precisas de utilizar uma base de dados **MySQL**.

1.  Utiliza o teu **Docker** para criar um servidor **MySQL**.

Bom Trabalho.