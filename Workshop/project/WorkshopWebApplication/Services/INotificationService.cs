﻿namespace WorkshopWebApplication.Services
{
    public interface INotificationService
    {
        Task SendMessageAsync(string title, string body);
    }
}