﻿namespace WorkshopWebApplication.Services
{
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    public class NotificationService : INotificationService
    {
        private readonly HttpClient _client;

        public NotificationService(HttpClient client)
        {
            _client = client;
        }

        public async Task SendMessageAsync(string title, string body)
        {
            var json = JsonConvert.SerializeObject(new { title, body });

            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("notify/apprise", data);

            var result = await response.Content.ReadAsStringAsync();

            Console.WriteLine(result);
        }
    }
}