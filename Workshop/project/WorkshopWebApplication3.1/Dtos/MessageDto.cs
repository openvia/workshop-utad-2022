﻿namespace WorkshopWebApplication3._1.Dtos
{
    public class MessageDto
    {
        public string Body { get; set; }
        public string Title { get; set; }
    }
}