﻿namespace WorkshopWebApplication3._1.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using WorkshopWebApplication3._1.Dtos;
    using WorkshopWebApplication3._1.Services;

    [ApiController]
    [Route("[controller]")]
    public class WorkshopController : ControllerBase
    {
        private readonly ILogger<WorkshopController> _logger;

        private readonly INotificationService _notificationService;

        public WorkshopController(
            INotificationService notificationService,
            ILogger<WorkshopController> logger)
        {
            _notificationService = notificationService;
            _logger = logger;
        }

        [HttpPost("SendMessage")]
        public async Task PostAsync(MessageDto message)
        {
            await _notificationService.SendMessageAsync(message.Title, message.Body);

            _logger.LogInformation($"Title:{message.Title} Body:{message.Body}", message);
        }
    }
}