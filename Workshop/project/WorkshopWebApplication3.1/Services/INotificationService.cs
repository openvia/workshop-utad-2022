﻿namespace WorkshopWebApplication3._1.Services
{
    using System.Threading.Tasks;

    public interface INotificationService
    {
        Task SendMessageAsync(string title, string body);
    }
}