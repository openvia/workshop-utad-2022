# Openvia Workshop 2022
## Setup Apprise (container Docker)

Para prosseguires vais ter de adicionar um **container** ao teu docker.

A imagem que vamos usar é o **Apprise**. O **Apprise** é uma imagem que permite, de forma simples, integrar com os principais serviços de notificação. Como **Discord**, slack, e-mail, Aws SNS, etc.

https://hub.docker.com/r/linuxserver/apprise-api

Para começar, vamos fazer download da imagem. Para isso, deves executar o seguinte comando na linha de comandos.

    docker pull linuxserver/apprise-api
    
Para criar o *container*, tens de executar o seguinte comando que irá "levantar" o **Apprise** na porta 8000 do teu computador.
    
    docker run -d --name=apprise-api -e PUID=1000 -e PGID=1000 -e TZ=Europe/London -p 8000:8000 -v /path/to/config:/config --restart unless-stopped lscr.io/linuxserver/apprise-api

Se tudo correu bem, deves conseguir aceder ao link http://localhost:8000/ onde irás ter acesso à interface grafica do teu container **Apprise**, como podes ver na imagem seguinte.

![](../resources/apprise1.png) 

## Setup Apprise (integração com o Discord)

Se até aqui correu tudo bem, vamos agora passar à integração com o Discord.


Para tal apenas tens de acede a http://localhost:8000/cfg/workshop e na aba *CONFIGURATION* colar o seguinte url. 
    
    https://discord.com/api/webhooks/951134263108067458/hmhQAQe5un79-MWXSiMcaahcFjtWRb_iNkslqhxD4XI--cx6qj0htUusF-xvCR5iYNmy
    
![](../resources/apprise2.png) 

Não te esqueças de fazer save das configurações.

Para confirmares que tudo correu bem, podes usar a aba *NOTIFICATIONS* e enviar uma notificação.

![](../resources/apprise3.png) 

Se tudo correu bem, deves ter recebido uma mensagem no canal do **Discord**





