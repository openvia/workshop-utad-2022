# Openvia Workshop 2022
## Projeto webAPI .net6 

Para teres acesso ao projeto (webAPI) que vamos integrar com o Discord, tens de aceder à seguinte pasta:

    openvia-workshop-2022\Workshop\project\WorkshopWebApplication

![](../resources/project1.png)

1. Abrir a solução  

Nesta pasta vais encontrar o ficheiro *WorkshopWebApplication.sln* e deves abri-lo com o **Visual Studio 2022**.

![](../resources/project2.png)

2. Conhecer e explora o código

Depois de abrires a solução no **Visual Studio 2022** deves explorar todos os ficheiros da mesma, tenta ler e interpretar o código, descobre o que faz. 
Se tiveres dúvidas não hesites em perguntar.

3. Correr o programa

Para continuar, vamos correr o programa. Para isso só tens de clicar em *run*.

![](../resources/project3.png)

4. Testar o programa

Se tudo correu bem, o programa abriu o browser com uma pagina igual à seguinte.

![](../resources/project4.png)

Agora testa o teu programa e verifica a tua messagem no **Discord**.

![](../resources/project5.png)




