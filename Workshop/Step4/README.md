# Openvia Workshop 2022
## Desafio 1

![](../resources/challenge.jpg)

Imagina que o programa deste Workshop pertence a uma aplicação web que lida com dados importantes e críticos e tu, 
como programador, queres ser notificado sempre que ocorra uma exceção na invocação da API.

1.  Abre uma nova branch para esta tarefa com o nome que achares apropriado.
2.  Modifica o programa de forma a enviar apenas as exceções para o canal do **Discord**.
3.  Provoca uma exceção e envia a mensagem para o **Discord**

Bom Trabalho.