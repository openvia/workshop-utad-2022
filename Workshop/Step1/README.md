# Openvia Workshop 2022
## Clone do workshop

O comando `git clone` é a ação que permite fazer download do projeto que está no repositório remoto (**GitLab**) para o repositório git local (na tua máquina).
 
Para clonares o projeto do **GitLab** para o teu computador deves seguir os seguintes passos:

1. Cria e abre uma pasta de destino para o projeto
  
2. Escreve `cmd` no caminho da pasta

![](../../Pre-Workshop/resources/Git/gitClone.png)

3. Escreve o seguinte comando:

    `$ git clone https://gitlab.com/openvia/workshop-dtech-2022.git`
    
    
    
    

  
  




