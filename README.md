![](Openvia_Logo.jpg) 

# Openvia Workshop 2022

## Tema do workshop:

Como preparar um ambiente de desenvolvimento em contexto Profissional – caso prático.

## Breve descrição do workshop:

Introdução a ambiente de desenvolvimento em contexto profissional, aplicação de ferramentas de desenvolvimento com recurso a ambientes virtualizados (ambiente Docker). 
Solução de integração com **Discord** através de API.

Objetivos: 
- Introdução a solução de **controlo de versões** 
- Introdução a ambiente de virtualização **Docker**


>  Junta-te ao Discord do Workshop em https://discord.gg/wjyrzCXc2e

0. [**Pre-Workshop**](../Pre-Workshop)
1. [**Clone do projeto**](../Workshop/Step1)
2. [**Setup Apprise (container Docker)**](../Workshop/Step2)
3. [**Projeto (WebAPI .net6)**](../Workshop/Step3)
4. [**Desafio 1**](../Workshop/Step4)
5. [**Desafio 2**](../Workshop/Step5)
