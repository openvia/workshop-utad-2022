![](Openvia_Logo.jpg) 

# Openvia Workshop 2022

Bem vindos ao Workshop da **Openvia** na **DTech 2022**!!!

Este workshop sob o tema ***“Como preparar ambiente de desenvolvimento em contexto Profissional – caso prático.”*** tem como principais objetivos os seguintes pontos:
* Introdução a solução de controlo de versões
* Introdução a ambiente de virtualização Docker


Para o Workshop, além de **computador** com ligação à **internet**, será necessario tambem que instales alguns softwares. 

Para te ajudar na instalação, elaboramos os seguintes tópicos:

0. [**Criação conta GitLab (opcional)**](GitLab)
1. [**Instalação git**](Git)
2. [**Instalação Docker**](Docker)
3. [**Instalação VisualStudio 2022**](VisualStudio)


