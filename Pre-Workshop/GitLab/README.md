# Openvia Workshop 2022

## 0. Criação de Conta GitLab (Opcional)

Se não conheces o GitLab vamos aproveitar a definição do Wikipédia

>  "GitLab is an open source code repository and collaborative software development platform for large DevOps and DevSecOps projects. GitLab is free for individuals."

Resumindo é um repositório de código.

Link para Criação de Conta: https://gitlab.com/users/sign_up

![](../resources/Git/gitCreateAccount.png) 


