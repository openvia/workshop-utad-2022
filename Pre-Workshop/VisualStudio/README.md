# Openvia Workshop 2022

## 2. Instalação Visual Studio 2022 

Se não conheces Visual Studio vamos aproveitar a definição do Wikipédia

>  "Microsoft Visual Studio is an integrated development environment (IDE) from Microsoft. It is used to develop computer programs, as well as websites, web apps, web services and mobile apps."

Resumindo, é um IDE para desenvolvimento de software.

Link para Download: https://visualstudio.microsoft.com/vs/community/

![](../resources/VisualStudio/visual1.png) 

![](../resources/VisualStudio/visual2.png)

![](../resources/VisualStudio/visual3.png)

![](../resources/VisualStudio/visual4.png)

![](../resources/VisualStudio/visual5.png) 

![](../resources/VisualStudio/visual6.png)

![](../resources/VisualStudio/visual7.png)

![](../resources/VisualStudio/visual8.png)

![](../resources/VisualStudio/visual9.png)

![](../resources/VisualStudio/visual10.png)
