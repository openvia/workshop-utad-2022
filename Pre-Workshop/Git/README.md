# Openvia Workshop 2022

## 1. Instalação Git

O Git é uma ferramenta que permite às equipas de desenvolvimento de software 
fazer uma gestão e um controlo de versões sobre o código desenvolvido,
permitindo que diferentes elementos  da equipa possam trabalhar em paralelo no mesmo serviço ou repositório.

Link para Download: https://gitforwindows.org/

Após o Download deves seguir os seguintes passos:

![](../resources/Git/git1.png) 

![](../resources/Git/git2.png)

![](../resources/Git/git3.png)

![](../resources/Git/git4.png)

![](../resources/Git/git5.png)

![](../resources/Git/git6.png)

![](../resources/Git/git7.png)

![](../resources/Git/git8.png)

![](../resources/Git/git9.png)

![](../resources/Git/git10.png)

![](../resources/Git/git11.png)

![](../resources/Git/git12.png)

![](../resources/Git/git13.png)

![](../resources/Git/git14.png)

![](../resources/Git/git15.png)

![](../resources/Git/git16.png)

![](../resources/Git/git17.png)

Para validar a instalação, deves aceder à linha de comandos e escrever:

    $ git --version 

![](../resources/Git/git18.png)

