![](Openvia_Logo.jpg) 

# Openvia Workshop 2022

1. Clone do projeto 
2. Docker Compose 
3. Projeto 
4. Swagger 



•             Tema do workshop:

Como preparar ambiente de desenvolvimento em contexto Profissional – caso prático.

•             Breve descrição do workshop:

Introdução a ambiente de desenvolvimento em contexto profissional, aplicação de ferramentas de desenvolvimento com recurso a ambientes virtualizados (ambiente Docker). 
Solução de Integração com Discord através de API.

Objetivos: 
- Introdução a solução de controlo de versões 
- Introdução a ambiente de virtualização Docker

Requisitos:
- Criar conta GitLab
- Instalação Git
- Instalação Docker
- Instalação Visual Studio
- Conta Discord

