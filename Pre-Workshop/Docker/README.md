# Openvia Workshop 2022

## 2. Instalação Docker 

Se não conheces Docker vamos aproveitar a definição do Wikipédia 
    
> "Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers."  

Resumindo é como uma máquina virtual mas sem o peso das maquinas virtuais tradicionais.

Link para Download: https://docs.docker.com/desktop/windows/install/

Após o Download deves seguir os seguintes passos:

![](../resources/Docker/docker1.png) 

![](../resources/Docker/docker2.png)

![](../resources/Docker/docker3.png)

![](../resources/Docker/docker4.png)

![](../resources/Docker/docker5.png) 

![](../resources/Docker/docker6.png)

![](../resources/Docker/docker7.png)

![](../resources/Docker/docker8.png)

![](../resources/Docker/docker9.png)

![](../resources/Docker/docker10.png)
